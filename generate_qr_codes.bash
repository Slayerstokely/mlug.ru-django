#!/bin/bash

i=1
while read -r line; do
	qrencode -o $1/qrcode_$i.png -s 20 $line
	montage -label 	$line $1/qrcode_$i.png -font /usr/share/fonts/truetype/ubuntu-font-family/Ubuntu-C.ttf -pointsize 18 -background White -geometry +0+0 $1/qrcode_$i.png
	let "i+=1"
done

cd $1
tar -cvf qrcodes.tar.gz *.png --remove-files
