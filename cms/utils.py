from math import ceil
from datetime import timedelta
import datetime
import calendar
import locale


def week_of_month (date):
	first_day = date.replace(day=1)
	dom = date.day
	adjusted_dom = dom + first_day.weekday()
	return int(ceil(adjusted_dom/7.0))


def nth_weekday (date, week, weekday):
	nth_weekday   = date.replace(day=1)
	adj           = (weekday - nth_weekday.weekday()) % 7
	nth_weekday  += timedelta(days=adj)
	nth_weekday  += timedelta(weeks=week-1)
	return nth_weekday


def get_next_2nd_friday_or_last_saturday (language, strf_format):
	if language == 'ru':
		locale.setlocale(locale.LC_ALL, "ru_RU.UTF-8")
	else:
		locale.setlocale(locale.LC_ALL, "en_US.UTF-8")
	today = datetime.date.today()

	if week_of_month(today) == 1 or (week_of_month(today) == 2 and today.weekday() <= 4):
		# meetup is on second friday
		next_2nd_friday_or_last_saturday = nth_weekday(today, 2, 4)

	else:
		# meetup is on last saturday
		calendar_month = calendar.monthcalendar(today.year, today.month)
		lastweek = calendar_month[-1]

		if lastweek[5]:
			# is there saturday on last week of current month?
			next_2nd_friday_or_last_saturday = nth_weekday(today, len(calendar_month), 5)
		else:
			next_2nd_friday_or_last_saturday = nth_weekday(today, len(calendar_month)-1, 5)

		if today > next_2nd_friday_or_last_saturday:
			# is last saturday anrealy in past?
			next_2nd_friday_or_last_saturday = nth_weekday( datetime.date( today.year, today.month + 1, 1 ), 2, 4)

	return next_2nd_friday_or_last_saturday.strftime(strf_format).decode('utf-8')


def get_hooks (content):
	result = []
	within_hook = False
	hook_start = 0
	for i in xrange(1, len(content)):
		if content[i] == content[i-1] == '{':
			within_hook = True
			hook_start = i-1
		if content[i] == content[i-1] == '}' and within_hook:
			within_hook = False
			result.append(content[hook_start:i+1])
	return result
