# -*- coding: utf-8 -*-
from django.utils.translation import ugettext as _
from django.contrib import admin
from models import Page
from django.conf import settings


class PageAdmin (admin.ModelAdmin):
	list_display = ('list_title', 'alias', 'date_created', 'author', 'is_published', 'is_in_menu')
	fieldsets = [
		(None,           {'fields': ['title', 'alias', 'content', ('header_includes', 'footer_includes'), ('author', 'is_published', 'is_in_menu')]}),
	]
	ordering = ['-date_created']
	list_filter = (
		('is_published', admin.BooleanFieldListFilter),
	)
	def view_on_site(self, obj):
		return '/cms/' + obj.alias


admin.site.index_title = _(u'Admin control unit')
admin.site.site_title = settings.SITE_NAME
admin.site.site_header = settings.SITE_NAME
admin.site.register(Page, PageAdmin)
