# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
from tinymce import models as tinymce_models
from django.utils.translation import ugettext as _
import datetime
import uuid

HTML = '<p></p>'


class Page (models.Model):
	title            = models.CharField(max_length=254, default="Yet another untitled page")
	alias            = models.CharField(max_length=254, default="", unique=True)
	content          = tinymce_models.HTMLField(default=HTML)
	header_includes  = models.TextField(default="", blank=True)
	footer_includes  = models.TextField(default="", blank=True)
	author           = models.ForeignKey(User)
	date_created     = models.DateTimeField(auto_now=True)
	is_published     = models.BooleanField(default=False)
	is_in_menu       = models.BooleanField(default=False)

	def list_title (self):
		return self.title
	list_title.short_description = _(u'Title')

	def __unicode__ (self):
		return self.title
