from django import template
from cms.models import Page

register = template.Library()


@register.assignment_tag
def get_pages_for_menu ():
	pages = Page.objects.only('is_in_menu', 'alias', 'title').filter(is_in_menu=True)
	result = []
	for page in pages:
		result.append((page.alias, page.title))
	return result
