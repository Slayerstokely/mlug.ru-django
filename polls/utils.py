# -*- coding: utf-8 -*-
from django.middleware.csrf import get_token
from django.utils.translation import ugettext as _
from django.conf import settings
from models import Poll, Option, Token
import urllib
import json
import re
uuid4hex = re.compile('[0-9a-f]{12}4[0-9a-f]{3}[89ab][0-9a-f]{15}\Z', re.I)


def assemble_poll_body (poll, token):
	if not poll.is_open:
		if poll.show_results:
			return assemble_poll_options(poll, 'results')
		else:
			return '<p>' + _(u'Poll closed') + '</p>'

	if poll.token_required and not token:
		if poll.show_results:
			return assemble_poll_options(poll, 'results')
		else:
			return assemble_poll_options(poll, 'disabled')

	return assemble_poll_options(poll, 'enabled')


def assemble_poll_options (poll, mode):
	result  = '<ul class="radioset">'
	for option in Option.objects.filter(poll=poll):
		result += '<li>'
		if mode == 'results':
			result += option.value
			result += '<div class="votebar" data-votes-counter="' + str(option.votes_counter) + '"><span>' + str(option.votes_counter) + '</span></div>'
		else:
			result += '<input type="radio" name="option" value="' + str(option.id) + '"> ' + option.value
			if poll.show_results:
				result += '<div class="votebar" data-votes-counter="' + str(option.votes_counter) + '"><span>' + str(option.votes_counter) + '</span></div>'
		result += '</li>'
	result += '</ul>'
	if mode == 'enabled':
		result += '<input type="submit" value="' + _(u'Vote') + '">'
	if mode == 'disabled':
		result += '<input type="submit" value="' + _(u'313373 only') + '" disabled="disabled">'
	return result


def get_poll_by_uuid (poll_uuid):
	if not uuid4hex.match(poll_uuid):
		return Poll(title='this is not uuid')
	if not Poll.objects.filter(uuid=poll_uuid):
		return Poll(title='wrong poll uuid')
	return Poll.objects.get(uuid=poll_uuid)


def get_poll_form_html (request, poll_uuid, back_url):
	csrf_token = get_token(request)
	poll = get_poll_by_uuid(poll_uuid)

	result = (
		'<form class="smallform poll" action="/polls/" method="POST"><input type="hidden" name="csrfmiddlewaretoken" value="' + csrf_token + '">'
		+ '<input type="hidden" name="uuid" value="' + poll_uuid + '">'
		+ '<input type="hidden" name="back_url" value="' + back_url + '">'
		+ '<p><strong>' + poll.title + '</strong></p>'
		+ '<p>' + poll.text + '</p>'
		+ assemble_poll_body(poll, request.COOKIES.get('token'))
		+ '</form>'
	)
	return result


def get_poll_results_html (poll_uuid):
	poll = get_poll_by_uuid(poll_uuid)
	result = (
		'<form class="smallform poll">'
		+ '<p><strong>' + poll.title + '</strong></p>'
		+ '<p>' + poll.text + '</p>'
		+ assemble_poll_options(poll, 'results')
		+ '</form>'
	)
	return result


def token_url_for_email (back_url):
	token = Token.objects.create(purpose="email")
	result = 'https://' + settings.SITE_DOMAIN + '/polls/activate_token?token=' + str(token.uuid) + '&redirect_url=' + urllib.quote(back_url)
	return result


def get_tokens_from_cookies (cookies):
	result = []
	if cookies.get('token'):
		try:
			result = json.loads(cookies.get('token'))
		except ValueError, e:
			pass
	if type(result) != list: result = []
	return result


def count_cow_powers (request):
	cow_powers_tokens = get_tokens_from_cookies(request.COOKIES)
	cow_powers = len(cow_powers_tokens)
	return {'cow_powers': cow_powers, 'cow_powers_tokens': cow_powers_tokens}
