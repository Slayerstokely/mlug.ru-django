from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.utils.translation import ugettext as _
from models import Poll, Option, Token
from utils import uuid4hex, get_tokens_from_cookies
import uuid
import json


def activate_token (request):
	if not request.GET.get('token') or not request.GET.get('redirect_url'):
		return render(request, '404.html', {'message': _('Bad request')}, status=400)
	token_uuid = request.GET.get('token').replace('-', '')
	redirect_url = request.GET.get('redirect_url')
	if not uuid4hex.match(token_uuid) or not Token.objects.filter(uuid=token_uuid):
		return render(request, '403.html', {'message': _('Invalid token')}, status=403)
	result = redirect(redirect_url)

	token_cookie = get_tokens_from_cookies(request.COOKIES)

	if not token_uuid in token_cookie: token_cookie.append(token_uuid)
	result.set_cookie(key='token', value=json.dumps(token_cookie))
	return result


def select_token (cookies):
	result = {'token': False, 'leftovers': []}
	tokens = get_tokens_from_cookies(cookies)
	if not len(tokens):
		return result
	for token in tokens:
		if uuid4hex.match(token) and Token.objects.filter(uuid=token):
			result['leftovers'].append(token)
			result['token'] = token
	return result

def vote (request):
	if not request.POST.get('csrfmiddlewaretoken'):
		return render(request, '404.html', {'message': _('You should not access this page directly')}, status=405)
	if not request.POST.get('uuid'):
		return render(request, '404.html', {'message': _('Missing poll id')}, status=400)
	poll_uuid = request.POST.get('uuid')
	if not uuid4hex.match(poll_uuid) or not Poll.objects.filter(uuid=poll_uuid):
		return render(request, '404.html', {'message': _('Invalid poll id')}, status=400)
	poll = Poll.objects.get(uuid=poll_uuid)
	if poll.token_required:
		token = select_token(request.COOKIES)
		if not token['token']:
			result = render(request, '403.html', {'message': _('You shall not pass')}, status=403)
			result.set_cookie(key='token', value=json.dumps(token['leftovers']))
			return result

	if not request.POST.get('option') or not Option.objects.filter(id=request.POST.get('option')):
		return render(request, '404.html', {'message': _('Invalid option id')}, status=400)
	option = Option.objects.get(id=request.POST.get('option'))
	if option.poll != poll:
		return render(request, '404.html', {'message': _('Option id mismatch')}, status=400)

	option.votes_counter += 1
	option.save()
	token_deleted = False
	if poll.token_required:
		db_token = Token.objects.get(uuid=token['token'])
		db_token.delete()
		token_deleted = True

	response = redirect(request.POST.get('back_url') or '/')
	if token_deleted:
		token['leftovers'].remove(token['token'])
		response.set_cookie(key='token', value=json.dumps(token['leftovers']))
	return response
