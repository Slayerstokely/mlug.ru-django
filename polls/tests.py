from django.test import TestCase, Client
from django.contrib.auth.models import User
from django.core.management import call_command
from models import Poll, Option, Token
import json
import views
import utils

c = Client(enforce_csrf_checks=True)


class PollTests (TestCase):
	def test_activate_token_and_vote (self):
		"""
		Can I activate token and vote?
		"""
		poll_1 = Poll.objects.create(
			title           = u'meow1',
			text            = u'meow1',
			token_required  = True,
		)
		option_1 = Option.objects.create(
			poll = poll_1,
			value = u'meow1',
		)
		poll_2 = Poll.objects.create(
			title           = u'meow2',
			text            = u'meow2',
		)
		option_2 = Option.objects.create(
			poll = poll_2,
			value = u'meow2',
		)
		token = Token.objects.create(purpose=u'test')
		response = c.get('/polls/')
		self.assertEqual(response.status_code, 405)
		response = c.get('/')
		response = c.post('/polls/', {
			'csrfmiddlewaretoken': c.cookies['csrftoken'].value,
			'uuid': str(poll_1.uuid).replace('-', ''),
			'option': option_1.id,
		})
		self.assertEqual(response.status_code, 403) # tokes is required but given none
		response = c.get('/polls/activate_token?token='+str(token.uuid)+'&redirect_url=/')
		self.assertEqual(response.status_code, 302) # token activated
		response = c.get('/')
		response = c.post('/polls/', {
			'csrfmiddlewaretoken': c.cookies['csrftoken'].value,
			'uuid': str(poll_1.uuid).replace('-', ''),
			'option': option_1.id,
		})
		self.assertEqual(response.status_code, 302) # successfully voted with token
		response = c.get('/')
		response = c.post('/polls/', {
			'csrfmiddlewaretoken': c.cookies['csrftoken'].value,
			'uuid': str(poll_1.uuid).replace('-', ''),
			'option': option_1.id,
		})
		self.assertEqual(response.status_code, 403) # token already used
		response = c.get('/')
		response = c.post('/polls/', {
			'csrfmiddlewaretoken': c.cookies['csrftoken'].value,
			'uuid': str(poll_2.uuid).replace('-', ''),
			'option': option_2.id,
		})
		self.assertEqual(response.status_code, 302) # successfully voted

		option_1 = Option.objects.get(value=u'meow1')
		option_2 = Option.objects.get(value=u'meow2')
		tokens_count = len(Token.objects.filter(purpose=u'test'))
		self.assertEqual(option_1.votes_counter, 1)
		self.assertEqual(option_2.votes_counter, 1)
		self.assertEqual(tokens_count, 0)

	def test_activate_5_tokens_and_vote_10_times (self):
		"""
		Can I activate multiple tokens and vote multiple times?
		"""
		poll_1 = Poll.objects.create(
			title           = u'meow1',
			text            = u'meow1',
			token_required  = True,
		)
		option_1 = Option.objects.create(
			poll = poll_1,
			value = u'meow1',
		)
		response = c.get('/polls/')
		token = Token.objects.create(purpose=u'test')
		response = c.get('/polls/activate_token?token='+str(token.uuid)+'&redirect_url=/')
		token = Token.objects.create(purpose=u'test')
		response = c.get('/polls/activate_token?token='+str(token.uuid)+'&redirect_url=/')
		token = Token.objects.create(purpose=u'test')
		response = c.get('/polls/activate_token?token='+str(token.uuid)+'&redirect_url=/')
		self.assertEqual(len(json.loads(c.cookies['token'].value)), 3)

		response = c.post('/polls/', {
			'csrfmiddlewaretoken': c.cookies['csrftoken'].value,
			'uuid': str(poll_1.uuid).replace('-', ''),
			'option': option_1.id,
		})
		response = c.post('/polls/', {
			'csrfmiddlewaretoken': c.cookies['csrftoken'].value,
			'uuid': str(poll_1.uuid).replace('-', ''),
			'option': option_1.id,
		})
		response = c.post('/polls/', {
			'csrfmiddlewaretoken': c.cookies['csrftoken'].value,
			'uuid': str(poll_1.uuid).replace('-', ''),
			'option': option_1.id,
		})
		self.assertEqual(len(json.loads(c.cookies['token'].value)), 0) # last token used

		response = c.post('/polls/', {
			'csrfmiddlewaretoken': c.cookies['csrftoken'].value,
			'uuid': str(poll_1.uuid).replace('-', ''),
			'option': option_1.id,
		})
		self.assertEqual(response.status_code, 403) # no tokens left

		response = c.post('/polls/', {
			'csrfmiddlewaretoken': c.cookies['csrftoken'].value,
			'uuid': str(poll_1.uuid).replace('-', ''),
			'option': option_1.id,
		})
		self.assertEqual(response.status_code, 403) # no tokens left

		token = Token.objects.create(purpose=u'test')
		response = c.get('/polls/activate_token?token='+str(token.uuid)+'&redirect_url=/')
		self.assertEqual(len(json.loads(c.cookies['token'].value)), 1) # one token
		response = c.post('/polls/', {
			'csrfmiddlewaretoken': c.cookies['csrftoken'].value,
			'uuid': str(poll_1.uuid).replace('-', ''),
			'option': option_1.id,
		})
		self.assertEqual(response.status_code, 302) # successfully voted with token
		response = c.post('/polls/', {
			'csrfmiddlewaretoken': c.cookies['csrftoken'].value,
			'uuid': str(poll_1.uuid).replace('-', ''),
			'option': option_1.id,
		})
		self.assertEqual(response.status_code, 403) # no tokens left

		token = Token.objects.create(purpose=u'test')
		response = c.get('/polls/activate_token?token='+str(token.uuid)+'&redirect_url=/')
		response = c.get('/polls/activate_token?token='+str(token.uuid)+'&redirect_url=/')
		response = c.get('/polls/activate_token?token='+str(token.uuid)+'&redirect_url=/')
		response = c.get('/polls/activate_token?token='+str(token.uuid)+'&redirect_url=/')
		response = c.get('/polls/activate_token?token='+str(token.uuid)+'&redirect_url=/')
		response = c.get('/polls/activate_token?token='+str(token.uuid)+'&redirect_url=/')
		response = c.get('/polls/activate_token?token='+str(token.uuid)+'&redirect_url=/')
		self.assertEqual(len(json.loads(c.cookies['token'].value)), 1) # one token, no matter how many times you try to cativate it
		response = c.post('/polls/', {
			'csrfmiddlewaretoken': c.cookies['csrftoken'].value,
			'uuid': str(poll_1.uuid).replace('-', ''),
			'option': option_1.id,
		})
		self.assertEqual(response.status_code, 302) # successfully voted with token
		response = c.post('/polls/', {
			'csrfmiddlewaretoken': c.cookies['csrftoken'].value,
			'uuid': str(poll_1.uuid).replace('-', ''),
			'option': option_1.id,
		})
		self.assertEqual(response.status_code, 403) # no tokens left
		self.assertEqual(len(json.loads(c.cookies['token'].value)), 0) # all tokens used

	def test_utils (self):
		"""
		Is every util works?
		"""
		poll = Poll.objects.create(
			title           = u'meow',
			text            = u'meow',
			token_required  = True,
		)
		option = Option.objects.create(
			poll = poll,
			value = u'meow',
		)
		request = c.get('/')
		result1 = utils.assemble_poll_body(poll, '')
		result2 = utils.get_poll_results_html(str(poll.uuid).replace('-', ''))
		result3 = utils.token_url_for_email ('/')
		self.assertEqual('<ul class="radioset">' in result1, True)
		self.assertEqual('<form class="smallform poll">' in result2, True)
		self.assertEqual('/polls/activate_token?token=' in result3, True)

	def test_token_links_generation (self):
		"""
		Does mass token generation works?
		"""
		call_command('generate_tokens', '--token_count', '3', '--redirect', '/')
		token_count = len(Token.objects.all())
		self.assertEqual(token_count, 3)
