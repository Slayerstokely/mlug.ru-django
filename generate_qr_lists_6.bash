#!/bin/bash

page=1
image=1
while read -r line; do
	qrencode -o $1/qrcode_$image.png -s 20 $line
	montage -label $line $1/qrcode_$image.png \
	-font /usr/share/fonts/truetype/ubuntu-font-family/Ubuntu-C.ttf \
	-pointsize 18 -background White -geometry 980x1005 $1/qrcode_$image.png
	if [ $image == 6 ]
	then
		montage \
                $1/qrcode_1.png  $1/qrcode_2.png  $1/qrcode_3.png  $1/qrcode_4.png  $1/qrcode_5.png  $1/qrcode_6.png \
		-tile 2x3 -geometry +50+50 $1/qr_page$page.png
		let "page+=1"
		let "image=0"
	fi
	let "image+=1"
done

cd $1
rm $1/qrcode_1.png  $1/qrcode_2.png  $1/qrcode_3.png  $1/qrcode_4.png  $1/qrcode_5.png  $1/qrcode_6.png

tar -cvf qrcodes.tar.gz *.png --remove-files
