# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from django.utils.translation import ugettext as _
from django.middleware.csrf import get_token
from settings import APP_NAME, APP_URL
from models import List


def validate_email (email):
	from django.core.validators import validate_email
	from django.core.exceptions import ValidationError
	try:
		validate_email(email)
		return True
	except ValidationError:
		return False


def get_list_of_lists ():
	lists = []
	for list in List.objects.all():
		if list.public:
			lists.append((list.name, list.uuid, list.default))
	return lists


def get_subsrcibe_form_html (request):
	csrf_token = get_token(request)
	result = '<form class="smallform" action="/' + APP_URL + '/subscribe" method="POST"><input type="hidden" name="csrfmiddlewaretoken" value="' + csrf_token + '">'
	for list in get_list_of_lists():
		result += '<input type="checkbox" name="list_' + str(list[1]) + '"'
		if list[2]: result += ' checked="checked"'
		result += '/>' + str(list[0]) + '<br />'
	result += '<input class="longinput" type="text" name="email" value="" placeholder="' + _(u'Email address') + '"><input type="submit" value="' + _(u'Subscribe') + '"></form>'
	return result
