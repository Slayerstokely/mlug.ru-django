(function(){
	function getRandomColor() {
		var letters = '0123456789ABCDEF'.split('');
		var color = '#';
		for (var i = 0; i < 6; i++ ) {
			color += letters[Math.floor(Math.random() * 16)];
		}
		return color;
	}

	polls = document.getElementsByClassName('poll');
	for (i=0; i<polls.length; i++){
		votebars = polls[i].getElementsByClassName('votebar');
		topvotes = 0;
		for (j=0; j<votebars.length; j++){
			if (topvotes < votebars[j].dataset.votesCounter) {
				topvotes = votebars[j].dataset.votesCounter;
			}
			votebars[j].style.backgroundColor = getRandomColor();
		}
		for (j=0; j<votebars.length; j++){
			votebars[j].style.width = (votebars[j].dataset.votesCounter*100 / topvotes) + '%';
		}
	}
})();
